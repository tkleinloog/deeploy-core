{{ define "imagePullSecret" }}
{{- printf "{\"auths\": {\"%s\": {\"username\": \"%s\",\"password\": \"%s\",\"auth\": \"%s\"}}}" .Values.images.registry .Values.images.username .Values.images.password (printf "%s:%s" .Values.images.username .Values.images.password | b64enc) | b64enc -}}
{{ end }}

{{ define "kubeConfig"}}
{{- printf "apiVersion: v1\nclusters:\n- cluster:\n    certificate-authority-data: <CERTIFICATE-AUTH>\n    server: https://kubernetes.default.svc\n  name: deeploy-cluster\ncontexts:\n- context:\n    cluster: deeploy-cluster\n    user: kserve\n  name: deeploy\ncurrent-context: deeploy\nkind: Config\npreferences: {}\nusers:\n- name: kserve\n  user:\n    token: <TOKEN>\n" | quote -}}
{{ end }}

{{ define "awsExtension" }}
{{- if (eq .Values.license.type "AWS") -}}
{{- if (eq .Values.license.aws.pricingModel "usage") -}}
-gn2zuq2nps5uriwzx00ooeto
{{- else if (eq .Values.license.aws.pricingModel "monthly") -}}
-doc8mazaxzirigf8urina0aml
{{- end -}}
{{- end -}}
{{ end }}

{{ define "awsProductCode" }}
{{- if (eq .Values.license.type "AWS") -}}
{{- if (eq .Values.license.aws.pricingModel "usage") -}}
{{- printf "gn2zuq2nps5uriwzx00ooeto" | quote -}}
{{ else if (eq .Values.license.aws.pricingModel "monthly") }}
{{- printf "doc8mazaxzirigf8urina0aml" | quote -}}
{{- end -}}
{{- end -}}
{{ end }}

{{ define "accountsHost" }}
{{- printf "europe-west3-deeploy-accounts.cloudfunctions.net" | quote -}}
{{ end }}

{{ define "accountsTLS" }}
{{- printf "true" | quote -}}
{{ end }}

{{ define "accountsPort" }}
{{- printf "443" | quote -}}
{{ end }}

{{ define "frontendEnv" }}
{{- printf "(function(window) {\n   window.__env = window.__env || {};\n   window.__env.production = true;\n   window.__env.repositoryServiceURL = 'https://api.%s';\n   window.__env.introspectorServiceUrl = 'https://api.%s';\n   window.__env.authorizationServiceUrl = 'https://api.%s';\n   window.__env.workspaceServiceURL = 'https://api.%s';\n   window.__env.kratosURL = 'https://api.%s/auth';\n   window.__env.deploymentServiceURL = 'https://api.%s';\n   window.__env.userServiceURL = 'https://api.%s';\n   window.__env.utilityServiceURL = 'https://api.%s';\n   window.__env.eventServiceURL = 'https://api.%s';\n  window.__env.eventGrpcServiceURL = 'https://api.%s';\n  window.__env.tokenServiceURL = 'https://api.%s';\n   window.__env.notificationServiceURL = 'https://api.%s';\n   window.__env.usageServiceURL = 'https://api.%s';\n   window.__env.licenseType = '%s';\n  \n})(this);\n" .Values.host .Values.host .Values.host .Values.host .Values.host .Values.host .Values.host .Values.host .Values.host .Values.host .Values.host .Values.host .Values.host .Values.license.type | quote -}}
{{ end }}
