# Deeploy Helm Chart

The Helm Chart for Deeploy, an MLOps platform for responsile ML deployments.

## Documentation

This Helm chart is part of the [complete installation guide](https://deeploy-ml.zendesk.com/hc/en-150/categories/360002889759-Install) and contains detailed instructions how to install the Deeploy Software Stack on Kubernetes using [Helm](https://helm.sh/) for production use.

## Installing the Chart

Before the chart can be installed the repo needs to be added to Helm, run the following commands to add the repo.

```
helm repo add deeploy-core https://deeploy-charts.storage.googleapis.com/
helm repo update
```

You can follow the detailed instructions in the [documentation](https://gitlab.com/deeploy-ml/deeploy-core/-/blob/master/README.md) which covers the Deeploy prerequisites and values file. You shoud result in something like the following command:

```
helm install deeploy deeploy-core/deeploy -f values.yaml --namespace deeploy --create namespace deeploy \
    --version 1.29.1
```

## Values

| Key | Type | Default | Description |
| --- | ---- |-------- | ----------- |
| `general.deeployNodeSelector` | Map | null | Node type to deploy the non-model resources of Deeploy. |
| `images.registry` | String  | `709825985650.dkr.ecr.us-east-1.amazonaws.com/deeploy/` | The registry to pull the Deeploy images from. Alternative for "Deeploy" License type: `docker.io` |
| `images.path` | String  | `/deeploy` | The path to the DeeployML registry. Alternative for "Deeploy" License type: `/deeployml` |
| `images.tag` | String  | `1.29.1` | The version tag of the deeploy deployment. |
| `images.username` | String  | null | If the license type is `DEEPLOY`, the supplied username for the Docker registry. |
| `images.password` | String  | null | If the license type is `DEEPLOY`, the supplied password for the Docker registry. |
| `host` | String  | null | The hostname on which you will be running deeploy. |
| `ingress.enabled` | Boolean | `false` | If `true`, a Kubernetes ingress resource will be created for host and api.host domains |
| `ingress.ingressClass` | String | null | Optional: the ingressClass, can also be set with an annotation `kubernetes.io/ingress.class` |
| `ingress.tlsSecretName` | String | null | Optional: tlsSecretName in the `istio-system` namespace for both host and api.host domains |
| `ingress.annotations` | JSON | {} | Optional: additional annotations that can be set to configure the ingress resource (for example see values.yaml) |
| `license.type` | String  | `AWS` | The type of license. Either `AWS` (AWS Marketplace) or `DEEPLOY` (BYOL via https://deeploy.ml) |
| `license.deeployLicenseKey` | String  | null | If the license type is `DEEPLOY`, this is the supplied Deeploy license key. |
| `license.aws.pricingModel` | String  | `usage` | If the license type is "AWS", this is AWS marketplace product type with corresponding procing method that Deeploy uses for the installation. Only required if `license.type` is `AWS` |
| `license.aws.trustedIamRoleArn` | String  | null | If the license type is `AWS`, ARN of the trusted AWS IAM role with the proper access rights to S3. See [here](https://deeploy-ml.zendesk.com/hc/en-150/articles/360020946659-Step-6-Allow-AWS-Marketplace-to-access-the-cluster) for more information on the access rights. Step for of this article is automatically done by this helm chart. |
| `license.aws.region` | String  | `eu-central-1` | If the license type is "AWS", this is the region where your cluster resides. Only required if `license.type` is `AWS` |
| `runPreinstall.enabled` | Boolean | `false` | Whether to run the preinstall scripts to register Deeploy at https://my.deeploy.ml. |
| `email.smtpHost`| String  | null | The hostname of the smtp server. |
| `email.port` | Int | null | The port for use of the smtp server. |
| `email.username` | String  | null | The username to access the smtp server. |
| `email.password` | String  | null | The password to access the smtp server. |
| `email.fromAddress` | The email address for Deeploy to send emails from, i.e. `deeploy@example.com`. |
| `database.host` | String  | null | The hostname of the PostgreSQL database server. |
| `database.port` | Int | `5432` | The port for use of the database server. |
| `database.username` | String  | null | The username to access the database. **Note**: this user should have administrative rights on the Deeploy databases. |
| `database.password` | String  | null | The password to access the database server. |
| `database.ssl.enabled` | Boolean | `false` | Whether to enable SSL on the database. If `true`, must also set `database.ssl.ca`. |
| `database.ssl.ca` | String  | null | The CA of your database provider. Must be set if `database.ssl.enabled` is `true`. E.g. for AWS, see [this guide](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/UsingWithRDS.SSL.html). |
| `objectStorage.type` | String | `AWS_S3` | Storage service to use with Deeploy. One of `AWS_S3`, `GCS`, `AZURE`, `MINIO`. |
| `objectStorage.aws.bucketName` | String  | null | Name of the remote S3 storage bucket to use. |
| `objectStorage.aws.useEKSPodIdentityWebhook` | Boolean | `false` | Wheter to use AWS IAM Role with EKS service accounts to authenticate with AWS S3. Check [this](https://github.com/aws/amazon-eks-pod-identity-webhook) for more background. |
| `objectStorage.aws.trustedIamRoleArn` | String  | null | ARN of the trusted AWS IAM role with the proper access rights to S3. See [here](https://deeploy-ml.zendesk.com/hc/en-150/articles/360019910079-Step-2-Configure-the-AWS-Cloud-Resources) for more information on the access rights. If `license.type` is `AWS`, `objectStorage.aws.trustedIamRoleArn` is the expected to be the same as `license.aws.trustedIamRoleArn` if `objectStorage.aws.useEKSPodIdentityWebhook` is `true`. |
| `objectStorage.aws.s3AccessKey` | String  | null | The IAM access key for the S3 server. Only required if `objectStorage.type` is `AWS_S3`. |
| `objectStorage.aws.s3SecretKey` | String  | null | The IAM secret access key for the S3 server Only required if `objectStorage.type` is `AWS_S3`. |
| `objectStorage.aws.region` | String  | `eu-central-1` | Region of the S3 storage bucket. Only required if `objectStorage.type` is `AWS_S3`. |
| `objectStorage.azure.subscriptionId` | String  | null | The id of the subscription that hosts the storage account. Only required if `objectStorage.type` is `AZURE`. |
| `objectStorage.azure.containerName` | String  | null | The container name to use. Only required if `objectStorage.type` is `AZURE`. |
| `objectStorage.azure.storageAccountName` | String  | null | The name of the Azure Storage Account. Only required if `objectStorage.type` is `AZURE`. |
| `objectStorage.azure.tenantId` | String  | null | The Tenant ID of the Azure storage service. Only required if `objectStorage.type` is `AZURE`. |
| `objectStorage.azure.clientId` | String  | null | The Client ID of the Azure storage service. Only required if `objectStorage.type` is `AZURE`. |
| `objectStorage.azure.clientSecret` | String  | null | The Client Secret of the Azure storage service. Only required if `objectStorage.type` is `AZURE`. |
| `objectStorage.gcp.serviceAccountKeyJson` | String  | null | The json file with the credentials for the GCP service account. Only required if `objectStorage.type` is `GCS`. |
| `objectStorage.gcp.bucketName` | String  | null | name of the remote GS storage bucket to use. |
| `objectStorage.minio.domain` | String  | null | The (local) uri to reach Minio on. Only required if `objectStorage.type` is `MINIO`. |
| `objectStorage.minio.port` | String  | `9000` | The port to reach Minio on. Only required if `objectStorage.type` is `MINIO`. |
| `objectStorage.minio.accessKey` | String  | null | The IAM access key for the S3 server. Only required if `objectStorage.type` is `MINIO` |
| `objectStorage.minio.secretKey` | String  | null | The IAM secret access key for the S3 server Only required if `objectStorage.type` is `MINIO`. |
| `security.tls.enabled` | Boolean | `true` | Whether to enable TLS **Note**: certificates required in `istio-system` namespace. |
| `security.keyManagement.kmsType` | String  | `AWS` | Either `AWS`, `AZURE` or `GCP`. |
| `security.keyManagement.aws.keyId` | String  | null | ID of a KMS key used to encrypt/decrypt. Only required if `security.keyManagement.kmsType` is `AWS`. |
| `security.keyManagement.aws.useEKSPodIdentityWebhook` | Boolean | `false` | Wheter to use AWS IAM Role with EKS service accounts to authenticate with AWS KMS. Check [this](https://github.com/aws/amazon-eks-pod-identity-webhook) for more background. |
| `security.keyManagement.aws.trustedIamRoleArn` | String  | null | ARN of the trusted AWS IAM role with the proper access rights to KMS. See [here](https://deeploy-ml.zendesk.com/hc/en-150/articles/360019910079-Step-2-Configure-the-AWS-Cloud-Resources) for more information on the access rights. If `license.type` is `AWS`, `security.keyManagement.aws.trustedIamRoleArn` is the expected to be the same as `license.aws.trustedIamRoleArn` if `objectStorage.aws.useEKSPodIdentityWebhook` is `true`. |
| `security.keyManagement.aws.accessKey` | String  | null | IAM Access Key of an account that has access to the key. Only required if `security.keyManagement.kmsType` is `AWS`. |
| `security.keyManagement.aws.secretKey` | String  | null  | IAM Secret Key of an account that has access to the key. Only required if `security.keyManagement.kmsType` is `AWS`. |
| `security.keyManagement.aws.region` | String  | `eu-central-1` | Region of the KMS. Only required if `security.keyManagement.kmsType` is `AWS`. |
| `security.keyManagement.azure.keyId` | String  | null | ID of the Azure Vault key. Only required if `security.keyManagement.kmsType` is `AZURE`. |
| `security.keyManagement.azure.vaultName` | String  | null | Name of the Azure vault. Only required if `security.keyManagement.kmsType` is `AZURE`. |
| `security.keyManagement.azure.clientId` | String  | null | The Client ID of the client using the Azure Vault service. Only required if `security.keyManagement.kmsType` is `AZURE`. |
| `security.keyManagement.azure.clientSecret` | String  | null | the Client Secret of the client using the Azure Vault service. Only required if `security.keyManagement.kmsType` is `AZURE`. |
| `security.keyManagement.azure.tenantId` | String  | null | The Tenant ID of the Azure Vault service. Only required if `security.keyManagement.kmsType` is `AZURE`. |
| `security.keyManagement.gcp.projectId` | String  | null | Google Cloud project ID (e.g. 'my-project'). Only required if `security.keyManagement.kmsType` is `GCP`. |
| `security.keyManagement.gcp.locationId` | String  | `eu-west4` | Cloud KMS location (e.g. 'eu-west4'). Only required if `security.keyManagement.kmsType` is `GCP`. |
| `security.keyManagement.gcp.keyRingId` | String  | null | ID of the Cloud KMS key ring (e.g. 'my-key-ring'). Only required if `security.keyManagement.kmsType` is `GCP`. |
| `security.keyManagement.gcp.keyId` | String  | null | ID of the key to use (e.g. 'my-key'). Only required if `security.keyManagement.kmsType` is `GCP`. |
| `security.keyManagement.gcp.serviceAccountKeyJson` | String  | null | The json file with the credentials for the GCP service account. Only required if `security.keyManagement.kmsType` is `GCS`. |
| `security.keyManagement.gcp.versionId` | String  | null | ID of the key version. Only required if `security.keyManagement.kmsType` is `GCP`. |
| `security.adminCredentials.firstName` | String  | null | First name of the main admin user. |
| `security.adminCredentials.lastName` | String  | null | Last name of the main admin user. |
| `security.adminCredentials.email` | String  | null | email of the main admin user. |
| `deploymentBackend.kserve.enabled` | Boolean | `true` | Whether to install the resource for the Deeploy Kserve deployment backend. |
| `deploymentBackend.sagemaker.enabled` | Boolean | `true` | Whether to install the resource for the Deeploy Sagemaker deployment backend. |
| `rabbitmq.auth.username` | String `deeploy`| |
| `rabbitmq.auth.existingPasswordSecret` | String  | `rabbitmq` | Existing Kubernetes Secret. The Deeploy Helm Chart created the `rabbitmq` secret with a random secret of length 30. |
| `rabbitmq.auth.existingErlangSecret` | String  | `rabbitmq` | Existing Kubernetes Secret. The Deeploy Helm Chart created the `rabbitmq` secret with a random secret of length 32. |
| `rabbitmq.persistence.enabled` | Boolean | `false` | |
