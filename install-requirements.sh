#!/bin/bash

# Versions
export ISTIO_VERSION=1.15.0
export KNATIVE_VERSION=knative-v1.6.0
export CERT_MANAGER_VERSION=v1.8.2

# Optional settings
export ISTIO_INJECTION_ENABLED=false
export KIALI_ENABLED=false
export PROMETHEUS_ENABLED=false
export GRAFANA_ENABLED=false

KUBE_VERSION=$(kubectl version --short=true)

echo "The Kubernetes version is 1.${KUBE_VERSION:43:2}"

# Install Istio for Knative https://knative.dev/docs/install/installing-istio/#installing-istio-with-sidecar-injection
# Default installation without sidecar injection
# If you wish to install Istio with an external load balancer set the following value --set values.gateways.istio-ingressgateway.type=NodePort
curl -L https://istio.io/downloadIstio | sh -
istio-${ISTIO_VERSION}/bin/istioctl install -y
# Enable istio-injection in istio-system namespace (optional, default false):
if [ "$ISTIO_INJECTION_ENABLED" = true ]; then kubectl label namespace istio-system istio-injection=enabled; else echo "Sidecar injection for Istio is not enabled"; fi
# Enable kiali addon (optional, default false):
if [ "$KIALI_ENABLED" = true ]; then kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.12/samples/addons/kiali.yaml; else echo "Kiali integraton for Istio is not enabled"; fi
# Enable prometheus addon (optional, default false):
if [ "$PROMETHEUS_ENABLED" = true ]; then kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.12/samples/addons/prometheus.yaml; else echo "Prometheus integraton for Istio is not enabled"; fi
# Enable grafana addon (optional, default false):
if [ "$GRAFANA_ENABLED" = true ]; then kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.12/samples/addons/grafana.yaml; else echo "Grafana integraton for Istio is not enabled"; fi

# Install Knative
kubectl apply -f https://github.com/knative/serving/releases/download/${KNATIVE_VERSION}/serving-crds.yaml
kubectl apply -f https://github.com/knative/serving/releases/download/${KNATIVE_VERSION}/serving-core.yaml
# Allow Deeploy to select nodes for serverless kserve deployments
kubectl patch configmap/config-features \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"kubernetes.podspec-nodeselector": "enabled"}}'
kubectl apply -f https://github.com/knative/net-istio/releases/download/${KNATIVE_VERSION}/net-istio.yaml
# Create knative local gateway
kubectl patch configmap/config-istio \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"local-gateway.knative-serving.knative-local-gateway": "knative-local-gateway.istio-system.svc.cluster.local"}}'
echo $(kubectl --namespace istio-system get service istio-ingressgateway)

# Install Cert-manager
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/${CERT_MANAGER_VERSION}/cert-manager.yaml
kubectl wait --for=condition=available --timeout=600s deployment/cert-manager-webhook -n cert-manager

# Clean up
rm -rf istio-${ISTIO_VERSION}
